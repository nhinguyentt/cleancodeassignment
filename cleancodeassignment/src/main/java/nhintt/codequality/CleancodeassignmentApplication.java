package nhintt.codequality;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleancodeassignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(CleancodeassignmentApplication.class, args);
	}
}
